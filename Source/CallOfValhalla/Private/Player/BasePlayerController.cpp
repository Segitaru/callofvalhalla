// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Player/BasePlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Net/UnrealNetwork.h"
#include "../CallOfValhalla.h"
#include "Engine/World.h"
#include "../Public/Character/BasePlayerCharacter.h"

ABasePlayerController::ABasePlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
void ABasePlayerController::BeginPlay()
{
	Super::BeginPlay();
	PlayerPawn = Cast<ABasePlayerCharacter>(GetPawn());
}
void ABasePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ABasePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ABasePlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ABasePlayerController::OnSetDestinationReleased);
	InputComponent->BindAxis("MoveForward", this, &ABasePlayerController::OnMoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABasePlayerController::OnMoveRight);
	InputComponent->BindAxis("CameraDistanceChange", this, &ABasePlayerController::ChangeCameraDistance);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ABasePlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ABasePlayerController::MoveToTouchLocation);
}



void ABasePlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (ABasePlayerCharacter* MyPawn = Cast<ABasePlayerCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void ABasePlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ABasePlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ABasePlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ABasePlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void ABasePlayerController::OnMoveForward(float AxisValue)
{
	if (AxisValue != 0) 
	{
		if (PlayerPawn && IsLocalController())
		{
			FVector Delta = PlayerPawn->CurrentCursor->GetComponentLocation() - PlayerPawn->GetActorLocation();
		
			PlayerPawn->AddMovementInput(Delta, AxisValue);
	
			//PlayerPawn->AddMovementInput(FVector(1.0f, 0.f, 0.f), AxisValue);
		}		
	}
	
}

void ABasePlayerController::OnMoveRight(float AxisValue)
{
	if (AxisValue != 0)
	{
	
		if (PlayerPawn && IsLocalController())
		{
			FVector Delta = PlayerPawn->CurrentCursor->GetComponentLocation() - PlayerPawn->GetActorLocation();

			PlayerPawn->AddMovementInput(FVector(Delta.X * 0.1, PlayerPawn->GetActorRightVector().Y + 1000, 0.f), AxisValue);

			//PlayerPawn->AddMovementInput(FVector(0.0f, 1.f, 0.f), AxisValue);
		}
	}
	
}

void ABasePlayerController::ChangeCameraDistance(float AxisValue)
{
	if (PlayerPawn && IsLocalController())
	{
		if (AxisValue != 0)
		{
			if (AxisValue > 0)
			{
				PlayerPawn->CameraHeight = PlayerPawn->CameraHeight + 100;
				PlayerPawn->SetTimerSmoothCamera();
			}
			else
			{
				PlayerPawn->CameraHeight = PlayerPawn->CameraHeight - 100;
				PlayerPawn->SetTimerSmoothCamera();
			}
		}
		
	}
}

void ABasePlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	PlayerPawn = Cast<ABasePlayerCharacter>(InPawn);
}

void ABasePlayerController::OnUnPossess()
{
	Super::OnUnPossess();

	PlayerPawn = nullptr;
}

void ABasePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ABasePlayerController, PlayerPawn);
}
