// Copyright ICFHAG Studio. All rights reserved. 2021


#include "COVStateEffect.h"
#include "../Public/Character/COVHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../Public/COVInterfaceGameActor.h"
#include "Net/UnrealNetwork.h"
bool UCOVStateEffect::InitObject(AActor* Actor, FName Bone)
{
	myActor = Actor;
	Bone = BoneToAttachedEffect;
	if (GetWorld())
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UCOVStateEffect::DestroyObject, Timer, false);
	ICOVInterfaceGameActor* myInterface = Cast<ICOVInterfaceGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	

	return true;
}


void UCOVStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UCOVStateEffect, BoneToAttachedEffect);
}
void UCOVStateEffect::DestroyObject()
{
	/*if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}*/
	ICOVInterfaceGameActor* myInterface = Cast<ICOVInterfaceGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	if (GetWorld())
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}

	
}

bool UCOVStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName Bone)
{
	Super::InitObject(Actor, Bone);
	ExecuteOnce();
	return true;
}

void UCOVStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UCOVStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UCOVHealthComponent* myHealthComp = Cast<UCOVHealthComponent>(myActor->GetComponentByClass(UCOVHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth_OnServer(Power);
		}
	}

}

bool UCOVStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName Bone)
{
	Super::InitObject(Actor, Bone);
	if(GetWorld())
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UCOVStateEffect_ExecuteTimer::Execute, RateTime, true);

	
	return true;
}

void UCOVStateEffect_ExecuteTimer::DestroyObject()
{
	
	Super::DestroyObject();
}

void UCOVStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UCOVHealthComponent* myHealthComp = Cast<UCOVHealthComponent>(myActor->GetComponentByClass(UCOVHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeCurrentHealth_OnServer(Power);
		}
	}
}
