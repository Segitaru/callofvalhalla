// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Game/COVGameInstance.h"

bool UCOVGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
    bool bIsFind = false;
    FWeaponInfo* WeaponInfoRow;
    WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
    if (WeaponInfoRow)
    {
        bIsFind = true;
        OutInfo = *WeaponInfoRow;
    }
    return bIsFind;
}

bool UCOVGameInstance::GetDropInfoByName(FName NameItem, FDropItem& OutInfo)
{
    bool bIsFind = false;
    if (DropItemInfoTable)
    {
        FDropItem* DropItemInfoRow;
        TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

        int8 i = 0;
        while (i<RowNames.Num() && !bIsFind)
        {
            DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
            if (DropItemInfoRow->WeaponSlotInfo.NameItem == NameItem)
            {
                OutInfo = (*DropItemInfoRow);
                bIsFind = true;
            }
            i++;
        }
      
    }
    return bIsFind;
}
