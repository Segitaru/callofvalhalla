// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Weapon/Projectile/Projectile_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugGrenadeShow = 0;
FAutoConsoleVariableRef CVarGrebadeShow(
	TEXT("COV.DebugGrenade"),
	DebugGrenadeShow,
	TEXT("Draw Debug for Grenade"),
	ECVF_Cheat);

void AProjectile_Grenade::BeginPlay()
{
	Super::BeginPlay();
}
void AProjectile_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerEsploded(DeltaTime);
}

void AProjectile_Grenade::Exploud()
{
	bTimerEnable = false;
	PlayFXGranade_Multicast(ProjectileSetting.ExploudSound, ProjectileSetting.ExploudeFX);
	TArray<AActor* > IgnoredActor;
	
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.fExploudMaxDamage, 
													ProjectileSetting.fExploudMaxDamage * 0.2f, GetActorLocation(), 
													ProjectileSetting.fProjectileMaxRadiusDamage, 
													ProjectileSetting.fProjectileMinRadiusDamage, 
													ProjectileSetting.fCoefDecreaseDamage,
													NULL, IgnoredActor, this, nullptr);
	//UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSetting.fProjectileDamage, Hit.Location, Hit.Location);
	if (DebugGrenadeShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.fProjectileMaxRadiusDamage, 8, FColor::Red, false, 4.f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.fProjectileMinRadiusDamage, 8, FColor::Green, false, 4.f);
	}

	this->Destroy();
}

void AProjectile_Grenade::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ProjectileCollisionSphereHit(HitComp, OtherActor,  OtherComp, NormalImpulse, Hit);
}

void AProjectile_Grenade::ImpactProjectile()
{
	bTimerEnable = true;
	TimeToExploud = ProjectileSetting.fTimeToExploud;
}

void AProjectile_Grenade::TimerEsploded(float DeltaTime)
{
	if (bTimerEnable)
	{
		if (TimerExploud >= TimeToExploud)
		{
			Exploud();
		}
		else
		{
			TimerExploud += DeltaTime;
		}
	}
}

void AProjectile_Grenade::PlayFXGranade_Multicast_Implementation(USoundBase* Sound, UParticleSystem* FX)
{
	if (FX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, GetActorLocation(), GetActorRotation(), GetActorScale() * fSizeFX);
	}
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
	}
}
