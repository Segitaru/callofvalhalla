// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/BasePlayerCharacter.h"

#include "HeadMountedDisplayFunctionLibrary.h" //
#include "Camera/CameraComponent.h" //
#include "UObject/ConstructorHelpers.h" //
#include "GameFramework/PlayerController.h" //
#include "GameFramework/SpringArmComponent.h" //
#include "Components/DecalComponent.h" //
#include "Kismet/GameplayStatics.h" //
#include "Kismet/KismetMathLibrary.h" //
#include "TimerManager.h"
#include "Components/SphereComponent.h"


#include "../Public/Character/COVCameraShake.h"
#include "../Public/Weapon/WeaponBase.h"
#include "../Public/Character/COVInventoryCharacter.h"
#include "../Public/Character/COVCharacterHealthComponent.h"
#include "../Public/Player/BasePlayerController.h"
#include "../Public/Game/COVGameInstance.h"


#include "../CallOfValhalla.h"

ABasePlayerCharacter::ABasePlayerCharacter()
{
	InteractableSphere = CreateDefaultSubobject<USphereComponent>(TEXT("InteractableSphere"));
	InteractableSphere->SetupAttachment(RootComponent);

	InventoryCharacter = CreateDefaultSubobject<UCOVInventoryCharacter>(TEXT("InventoryCharacter"));
	if (InventoryCharacter)
	{
		InventoryCharacter->OnSwitchWeapon.AddDynamic(this, &ABasePlayerCharacter::InitWeapon);
	}

	CharacterHealthComponent = CreateDefaultSubobject<UCOVCharacterHealthComponent>(TEXT("HealthCharacter"));
	if (CharacterHealthComponent)
	{
		CharacterHealthComponent->OnDead.AddDynamic(this, &ABasePlayerCharacter::DeathCharacter);
	}

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = CameraHeight;
	CameraBoom->SetRelativeRotation(FRotator(CameraAngle, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraBoom->bEnableCameraLag = Smooth;
	CameraBoom->CameraLagSpeed = SmoothFactor;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

void ABasePlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	// ����������� ������� �� ����
	if (CharacterHealthComponent && CharacterHealthComponent->GetIsAlive())
	{
		//��������� ����������
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			//FString SEnum = UEnum::GetValueAsString(MovementState);
			//UE_LOG(LogCOV_Net, Warning, TEXT("Movement State: %s"), *SEnum);
			if (CurrentCursor)
			{
				if (APlayerController* PC = Cast<APlayerController>(GetController()))
				{
					FHitResult TraceHitResult;
					PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
					FVector CursorFV = TraceHitResult.ImpactNormal;
					FRotator CursorR = CursorFV.Rotation();
					CurrentCursor->SetWorldLocation(TraceHitResult.Location);
					CurrentCursor->SetWorldRotation(CursorR);

					UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
					FRotator LookOnCursor = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TraceHitResult.Location);
					//Client
					SetActorRotation(FRotator(0.f, LookOnCursor.Yaw, 0.f));
					//Net
					SetActorRotationByYaw_OnServer(LookOnCursor.Yaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bShouldReduce = false;
						switch (MovementState)
						{
						case EMovementState::AimState:
							Displacement = FVector(0.f, 0.f, 160.f);
							//CurrentWeapon->bShouldReduceDispersion = true;
							bShouldReduce = true;
							break;
						case EMovementState::WalkState:
							Displacement = FVector(0.f, 0.f, 160.f);
							//CurrentWeapon->bShouldReduceDispersion = true;
							bShouldReduce = true;
							break;
						case EMovementState::RunState:
							Displacement = FVector(0.f, 0.f, 120.f);
							//CurrentWeapon->bShouldReduceDispersion = false;	
							break;
						case EMovementState::SprintState:
							Displacement = FVector(0.f, 0.f, 120.f);
							//CurrentWeapon->bShouldReduceDispersion = false;
							break;
						default:
							break;
						}
						//CurrentWeapon->EndLocationShoot = TraceHitResult.Location + Displacement;
						CurrentWeapon->UpdateWeaponEndLocationByCharacter_OnServer(TraceHitResult.Location + Displacement, bShouldReduce);
					}
				}
			}

			// ��� ���������� �������
			if (bSprintState)
			{
				float ForwardDeltaDegree;
				FMatrix RotMatrix = FRotationMatrix(GetActorRotation());
				FVector ForwardVector = RotMatrix.GetScaledAxis(EAxis::X);
				FVector RightVector = RotMatrix.GetScaledAxis(EAxis::Y);
				FVector NormalizedVel = GetVelocity().GetSafeNormal2D();

				// get a cos(alpha) of forward vector vs velocity
				float ForwardCosAngle = FVector::DotProduct(ForwardVector, NormalizedVel);
				// now get the alpha and convert to degree
				ForwardDeltaDegree = FMath::RadiansToDegrees(FMath::Acos(ForwardCosAngle));

				// depending on where right vector is, flip it
				float RightCosAngle = FVector::DotProduct(RightVector, NormalizedVel);
				if (RightCosAngle < 0)
				{
					ForwardDeltaDegree *= -1;
				}

				if (ForwardDeltaDegree < 55 && ForwardDeltaDegree > -55)
				{
					ChangeMovementState();
				}
				else
				{
					ReleasedSprintCharacter();
				}
			}
		}
		
	}
	
}

void ABasePlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	// ������� �������� ������
	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
				CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
	
}

void ABasePlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("AimState", IE_Pressed, this, &ABasePlayerCharacter::PressedAimCharacter);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ABasePlayerCharacter::PressedSprintCharacter);
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &ABasePlayerCharacter::PressedWalkCharacter);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ABasePlayerCharacter::PressedAttackCharacter);
	PlayerInputComponent->BindAction("CooldownWeapon", IE_Pressed, this, &ABasePlayerCharacter::PressedTryCooldownWeapon);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ABasePlayerCharacter::PressedNextWeapon);
	PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &ABasePlayerCharacter::PressedPreviousWeapon);
	PlayerInputComponent->BindAction("DropWeapon", IE_Pressed, this, &ABasePlayerCharacter::PressedDropWeapon);

	

	PlayerInputComponent->BindAction("AimState", IE_Released, this, &ABasePlayerCharacter::ReleasedAimCharacter);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ABasePlayerCharacter::ReleasedSprintCharacter);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &ABasePlayerCharacter::ReleasedWalkCharacter);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &ABasePlayerCharacter::ReleasedAttackCharacter);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<1>);
	PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<2>);
	PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<3>);
	PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<4>);
	PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<5>);
	PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<6>);
	PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<7>);
	PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<8>);
	PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<9>);
	PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ABasePlayerCharacter::TKeyPressed<0>);

}

void ABasePlayerCharacter::InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index)
{
	Super::InitWeapon(IdWeapon, AddicionalWeaponInfo, Index);

	if (InventoryCharacter)
	{
		InventoryCharacter->OnAmmoWeaponAvaible.Broadcast(CurrentWeapon->WeaponSetting.WeaponType);
	}
}

//void ABasePlayerCharacter::WeaponReloadEnd(bool IsSuccess, int32 AmmoSafe)
//{
//	Super::WeaponReloadEnd_Multicast(IsSuccess, AmmoSafe);
//	if (IsSuccess)
//	{
//		if (InventoryCharacter)
//		{
//			InventoryCharacter->WeaponChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
//			InventoryCharacter->SetAddicionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
//		}
//	}
//	
//}


void ABasePlayerCharacter::AdditionalActionForWeaponUse()
{
	if (InventoryCharacter && CurrentWeapon)
	{

		InventoryCharacter->SetAddicionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
	}
}

void ABasePlayerCharacter::DeathCharacter()
{
	Super::DeathCharacter();

	if(GetCursorToWorld())
	GetCursorToWorld()->SetVisibility(false);
}

void ABasePlayerCharacter::MakeCameraShake()
{
	if (CameraShake)
	{
		UGameplayStatics::PlayWorldCameraShake(GetWorld(), CameraShake, FVector(GetTopDownCameraComponent()->GetComponentLocation()), 0.f, 30.f, 1.f, false);
	}
}
void ABasePlayerCharacter::SetTimerSmoothCamera()
{
	GetWorld()->GetTimerManager().SetTimer(TimerCameraSmooth, this, &ABasePlayerCharacter::SmoothingCameraBoom, 0.01667f, true);
}
void ABasePlayerCharacter::SmoothingCameraBoom()
{
	
	if (GetCameraBoom()->TargetArmLength < MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);
		
	} 
	if (GetCameraBoom()->TargetArmLength > MaxDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MaxDistanceCamera;

	} 
	if (GetCameraBoom()->TargetArmLength > MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength, CameraHeight, 1.f, 0.02f);
	}
	if (GetCameraBoom()->TargetArmLength < MinDistanceCamera)
	{
		GetCameraBoom()->TargetArmLength = MinDistanceCamera;

	}	
}

void ABasePlayerCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->bWeaponCooldown && InventoryCharacter->WeaponsSlot.IsValidIndex(ToIndex))
	{
		if (CurrentWeaponIndex != ToIndex && InventoryCharacter)
		{
			int32 OldIndex = CurrentWeaponIndex;
			FAddicionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponInfo;
				if (CurrentWeapon->bWeaponCooldown)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryCharacter->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	/*return bIsSuccess;*/
}

void ABasePlayerCharacter::PressedWalkCharacter()
{
	bWalkState = true;
	ChangeMovementState();
}
void ABasePlayerCharacter::PressedSprintCharacter()
{
	bSprintState = true;
}
void ABasePlayerCharacter::PressedAimCharacter()
{
	bAimState = true;
	ChangeMovementState();
}
void ABasePlayerCharacter::PressedAttackCharacter()
{
	if(CharacterHealthComponent->GetIsAlive())
	CharacterAttackEvent(true);
}
void ABasePlayerCharacter::PressedTryCooldownWeapon()
{
	if (CharacterHealthComponent->GetIsAlive() && GetCurrentWeapon() && !GetCurrentWeapon()->bWeaponCooldown)
	{
		TryReloadWeaponFromPlayer_OnServer();
	}
}
void ABasePlayerCharacter::PressedNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponCooldown && InventoryCharacter->WeaponsSlot.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponCooldown)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryCharacter)
		{
			if (InventoryCharacter->SwitchWeaponToIndex(CurrentWeaponIndex + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}
void ABasePlayerCharacter::PressedPreviousWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->bWeaponCooldown && InventoryCharacter->WeaponsSlot.Num() > 1)
	{
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->bWeaponCooldown)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryCharacter)
		{
			if (InventoryCharacter->SwitchWeaponToIndex(CurrentWeaponIndex - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ABasePlayerCharacter::PressedDropWeapon()
{
	if (InventoryCharacter)
	{
		
		InventoryCharacter->DropWeaponByIndex_OnServer(CurrentWeaponIndex);
	}
}

void ABasePlayerCharacter::ReleasedWalkCharacter()
{
	bWalkState = false;
	ChangeMovementState();
}
void ABasePlayerCharacter::ReleasedSprintCharacter()
{
	bSprintState = false;
	ChangeMovementState();
}
void ABasePlayerCharacter::ReleasedAimCharacter()
{
	bAimState = false;
	ChangeMovementState();
}
void ABasePlayerCharacter::ReleasedAttackCharacter()
{
	CharacterAttackEvent(false);
}

UDecalComponent* ABasePlayerCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ABasePlayerCharacter::TryReloadWeaponFromPlayer_OnServer_Implementation()
{
		if(GetCurrentWeapon()->GetWeaponRound() < GetCurrentWeapon()->WeaponSetting.MaxRound && GetCurrentWeapon()->CanReuse())
		{
			GetCurrentWeapon()->InitCooldown();
		}
}

void ABasePlayerCharacter::AdditionalActionForWeaponReloadEnd(bool IsSuccess, int32 AmmoSafe)
{
	if (IsSuccess)
	{
		if (InventoryCharacter)
		{
			InventoryCharacter->WeaponChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
			InventoryCharacter->SetAddicionalWeaponInfo(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
		}
	}
}
