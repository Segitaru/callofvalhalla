// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/COVCharacterHealthComponent.h"

void UCOVCharacterHealthComponent::ChangeCurrentHealth_OnServer(float ChangeValue)
{
	if (ChangeValue < 0)
	{
		if (Shield > 0)
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryShield);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryHealth);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);

			ChangeCurrentShield(ChangeValue);

			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryShield, this, &UCOVCharacterHealthComponent::ShieldCooldownToRecovery, CooldownRecoveryShield, false);
		}
		else
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryShield);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownRecoveryHealth);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);

			Super::ChangeCurrentHealth_OnServer(ChangeValue);

			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryShield, this, &UCOVCharacterHealthComponent::ShieldCooldownToRecovery, CooldownRecoveryShield, false);
		}
	}
	else
	Super::ChangeCurrentHealth_OnServer(ChangeValue);
}

float UCOVCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UCOVCharacterHealthComponent::ChangeCurrentShield(float ChangeValue)
{
	Shield += ChangeValue*fDamageCoef*fDamageCoefShield;
	OnShieldChange_Multicast(Shield, ChangeValue);
	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else
	{
		if (Shield <= 0.f)
		{
			Shield = 0;
		}
	}
}

void UCOVCharacterHealthComponent::ShieldCooldownToRecovery()
{
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_RateShieldRecovery, this, &UCOVCharacterHealthComponent::ShieldRecovery, RateShieldRecovery, true);
}

void UCOVCharacterHealthComponent::ShieldRecovery()
{
	if (Shield < 100.f)
	{
		Shield += ValueRecoveryShield;
		OnShieldChange_Multicast(Shield, ValueRecoveryShield);
	}
	else
	{
		Shield = 100.f;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateShieldRecovery);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownRecoveryHealth, this, &UCOVCharacterHealthComponent::HealthCooldownToRecovery, CooldownRecoveryHealth, false);

	}

}

void UCOVCharacterHealthComponent::HealthCooldownToRecovery()
{

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_RateHealthRecovery, this, &UCOVCharacterHealthComponent::HealthRecovery, RateHealthRecovery, true);
}

void UCOVCharacterHealthComponent::HealthRecovery()
{

	if (Health < 100.f)
	{
		Health += ValueRecoveryHealth;
		OnHealthChange_Multicast(Health, ValueRecoveryHealth);
	}
	else
	{
		Health = 100.f;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_RateHealthRecovery);

	}
}

void UCOVCharacterHealthComponent::OnShieldChange_Multicast_Implementation(float NewShield, float Damage)
{
	OnShieldChange.Broadcast(NewShield, Damage);
}
