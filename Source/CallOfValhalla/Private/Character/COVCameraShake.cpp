// Copyright ICFHAG Studio. All rights reserved. 2021


#include "Character/COVCameraShake.h"

UCOVCameraShake::UCOVCameraShake()
{
	OscillationDuration = 0.25f;
	OscillationBlendInTime = 0.1f;
	OscillationBlendOutTime = 0.2f;
	RotOscillation.Pitch.Amplitude=5.f;
	RotOscillation.Pitch.Frequency = 50.f;
	RotOscillation.Yaw.Amplitude = 5.f;
	RotOscillation.Yaw.Frequency = 50.f;
	LocOscillation.X.Amplitude = 100.f;
	LocOscillation.X.Frequency = 1.f;
	LocOscillation.X.InitialOffset = EInitialOscillatorOffset::EOO_OffsetZero;
	LocOscillation.Z.Amplitude = 100.f;
	LocOscillation.Z.Frequency = 1.f;
	LocOscillation.Z.InitialOffset = EInitialOscillatorOffset::EOO_OffsetZero;
}