// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "UObject/NoExportTypes.h"
#include "COVStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class CALLOFVALHALLA_API UCOVStateEffect : public UObject
{
	GENERATED_BODY()
public:
	virtual bool IsSupportedForNetworking() const override { return true; };
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	virtual bool InitObject(AActor* Actor, FName Bone);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractableSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bAutoDestroy = false;
	AActor* myActor = nullptr;

	FTimerHandle TimerHandle_EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FName BoneToAttachedEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		FVector ScaleEmitter = FVector(1.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		float Timer = 5.0f;



};

UCLASS()
class CALLOFVALHALLA_API UCOVStateEffect_ExecuteOnce : public UCOVStateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName Bone) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};

UCLASS()
class CALLOFVALHALLA_API UCOVStateEffect_ExecuteTimer : public UCOVStateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName Bone) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	
};