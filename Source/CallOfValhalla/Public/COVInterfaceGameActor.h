// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Public/COVStateEffect.h"
#include "../Public/FuncLibrary/Types.h"
#include "COVInterfaceGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCOVInterfaceGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class CALLOFVALHALLA_API ICOVInterfaceGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	//	bool AvaiableForEffects();


	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UCOVStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UCOVStateEffect* RemoveEffect);
	virtual void AddEffect(UCOVStateEffect* newEffect);
	virtual void GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);

};
