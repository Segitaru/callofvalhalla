// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "../FuncLibrary/Types.h"
#include "../Weapon/WeaponBase.h"
#include "COVGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API UCOVGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropInfoByName(FName NameItem, FDropItem& OutInfo);
};
