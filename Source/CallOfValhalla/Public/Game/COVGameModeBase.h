// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "COVGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API ACOVGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

};
