// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/COVInterfaceGameActor.h"
#include "COVEnviromentActorBase.generated.h"

UCLASS()
class CALLOFVALHALLA_API ACOVEnviromentActorBase : public AActor, public ICOVInterfaceGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACOVEnviromentActorBase();
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	UPROPERTY(Replicated)
	TArray<class UCOVStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UCOVStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UCOVStateEffect* EffectRemove = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug")
		TArray<class UParticleSystemComponent*> ParticalEffects;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effect")
	FVector EffectOffset = FVector(0);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UCOVStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UCOVStateEffect* RemoveEffect) override;
	void AddEffect(UCOVStateEffect* newEffect) override;
	void GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone) override;

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(UCOVStateEffect* Effect, bool bAddOrRemove);
	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);
};
