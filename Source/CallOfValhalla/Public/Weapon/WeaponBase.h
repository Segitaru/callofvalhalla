// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Public/FuncLibrary/Types.h"
#include "WeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopUseStart, UAnimMontage*, AnimUseWeapon, float, ReUseTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeapopUseEnd, bool, IsSuccess);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopReloadStart, UAnimMontage*, AnimReload, float, ReUseTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeapopReloadEnd, bool, IsSuccess, int32, AmmoSafe);


UCLASS()
class CALLOFVALHALLA_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponBase();

	FOnWeapopUseStart OnWeapopUseStart;
	FOnWeapopUseEnd OnWeapopUseEnd;
	FOnWeapopReloadStart OnWeapopReloadStart;
	FOnWeapopReloadEnd OnWeapopReloadEnd;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* SkeletalMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UArrowComponent* ShootLocation = nullptr;
	
		FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAddicionalWeaponInfo WeaponInfo;

	//Stats
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reload")
		float CooldownTime = 0.0f;
	float fCurrentDispersion = 0.f;
	float fCurrentDispersionMax = 10.f;
	float fCurrentDispersionMin = 0.1f;
	float fCurrentDispersionRecoil = 0.1f;
	float fCurrentDispersionReduction = 0.1f;
	//Stats
	
	// Other
	UPROPERTY(Replicated)
		FVector EndLocationShoot;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.f;
		FName IdWeaponName;
	// Other

	//�����
		UPROPERTY(Replicated)
	bool bShouldReduceDispersion = false;
	bool DropMeshFlag = false;
	bool DropOtherMeshFlag = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Use logic")
		bool bWeaponUse = false;
		bool bBlockUse = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Use logic")
		bool bWeaponCooldown = false;
	
	UParticleSystemComponent* TraceEmittor = nullptr;
		bool DestroyTrace = false;
	//�����
	
	//�������
	float TimeTrace = 0.5f;
	float SpeedRate = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reload")
		float CooldownRate = 0.0f;
	float DropMeshTimer = 0.f;
	float DropOtherMeshTimer = 0.f;

	//�������


protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	void UseWeaponTick(float DeltaTime);
	void CooldownWeaponTick(float DeltaTime);
	void DispertionTick(float DeltaTime);
	void DropOtherMeshTick(float DeltaTime);
	void DropMeshTick(float DeltaTime);

	void WeaponInit();
	void InitCooldown();
	UFUNCTION(Server, UnReliable)
	void InitDropMesh_OnServer(UStaticMesh* MeshToDrop, FTransform Offset, FVector ImpulseDrop, float LifeTime, float ImpulseRandomDespersion, float PowerImpulse, float CustomMass);
	
	// ������

	void UseWeapon();
	void FinishCooldownToUse();
	void CancelReload();
	// ������
	
	// Change State

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState MovementState);
	FVector ApplyDisparcionToShoot(FVector DirectionShoot) const;
	void ChangeDispertionByShot();
	bool CanReuse();
	int8 GetAviableAmmoToReload();

	// Change State
	
	// GetSome...()

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();
		FVector GetShootEndLocation();
		bool CheckWeaponCanUsed();
		FProjectileInfo GetProjectile();
		float GetCurrentDispersion() const; 
		int8 GetNumberProjectileByShoot() const;

	// GetSome...()

		//NET
		virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

		UFUNCTION(Server, Unreliable)
			void UpdateWeaponEndLocationByCharacter_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDespersion);

		UFUNCTION(NetMulticast, Unreliable)
			void InitShellDrop_Multicast(UStaticMesh* MeshToDrop, FTransform Offset, FVector ImpulseDrop, float LifeTime, float ImpulseRandomDespersion, float PowerImpulse, float CustomMass, FVector LocalDir);
		UFUNCTION(NetMulticast, Unreliable)
			void PlaySoundUseWeapon_Multicast(USoundBase* Sound, FVector Location);
		UFUNCTION(NetMulticast, Unreliable)
			void PlayFXUseWeapon_Multicast(UParticleSystem* FX, FTransform Transform);

		UFUNCTION(NetMulticast, Reliable)
			void SpawnHitDecal_Multicast(UMaterialInterface* Decal, UPrimitiveComponent* OtherComp, FHitResult Hit);
		UFUNCTION(NetMulticast, Reliable)
			void SpawnHitFX_Multicast(UParticleSystem* FX, FHitResult Hit);
		UFUNCTION(NetMulticast, Reliable)
			void SpawnTraceEmmitor_Multicast(UParticleSystem* FXTrace, FRotator EmittorRotation);
		UFUNCTION(NetMulticast, Reliable)
			void SpawnHitSound_Multicast(USoundBase* Sound, FHitResult Hit);
		UFUNCTION(NetMulticast, Reliable)
			void DestroyTraceFXTick_Multicast(float DeltaTime);

};
