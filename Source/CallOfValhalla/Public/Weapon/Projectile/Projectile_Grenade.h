// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Projectile/ProjectileWeaponBase.h"
#include "Projectile_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API AProjectile_Grenade : public AProjectileWeaponBase
{
	GENERATED_BODY()
public:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void Exploud();
	virtual void ProjectileCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ImpactProjectile() override;
	void TimerEsploded(float DeltaTime);
	bool bTimerEnable = false;
	float TimerExploud = 0.0f;
	float TimeToExploud = 0.1f;

	UFUNCTION(NetMulticast, Reliable)
		void PlayFXGranade_Multicast(USoundBase* Sound, UParticleSystem* FX);
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX, meta = (AllowPrivateAccess = "true"))
		float fSizeFX = 1;
};
