// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "COVHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStatsParam
{
	GENERATED_BODY()


};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CALLOFVALHALLA_API UCOVHealthComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this component's properties
	UCOVHealthComponent();
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDead OnDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Coefficient")
		float fDamageCoef = 1.f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(Replicated)
	float Health = 100.f;
	UPROPERTY(Replicated)
	bool bIsAlive = true;

public:	

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
		bool GetIsAlive();

	//NET
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION(Server, Reliable,BlueprintCallable, Category = "Health")
		virtual void ChangeCurrentHealth_OnServer(float ChangeValue);

	UFUNCTION(NetMulticast, Unreliable)
		void OnHealthChange_Multicast(float NewHealth, float Damage);
	UFUNCTION(NetMulticast, Unreliable)
		void OnDead_Multicast();
};
