// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLibrary/Types.h"
#include "../Public/COVInterfaceGameActor.h"
#include "BaseCharacter.generated.h"

UCLASS()
class CALLOFVALHALLA_API ABaseCharacter : public ACharacter, public ICOVInterfaceGameActor
{
	GENERATED_BODY()
protected:
	class UCOVGameInstance* ReferenceGI = nullptr;
public:
	ABaseCharacter();
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Component")
		class UCOVHealthComponent* HealthComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Component")
		class UCOVCharacterHealthComponent* CharacterHealthComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Movement")
		EMovementState MovementState = EMovementState::RunState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed SpeedState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bCanMove;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bCanAttack;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalkState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bSprintState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bAimState;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool bRightHandState;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool bMeleeWeaponState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathState")
		TArray<UAnimMontage*> DeathMontage;

		FTimerHandle TimerHandle_Rackdoll;
	UFUNCTION(NetMulticast, Reliable)
		void EnableRackdoll_Multicast();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FName InitWeaponName;
	UPROPERTY(Replicated, VisibleAnyWhere, BlueprintReadOnly, Category = "Weapon")
		AWeaponBase* CurrentWeapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		bool bReUseWeapon = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		bool bUseWeapon = false;
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		int32 CurrentWeaponIndex = 0;


	UPROPERTY(Replicated)
	TArray<class UCOVStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UCOVStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UCOVStateEffect* EffectRemove = nullptr;
	UFUNCTION(BlueprintPure, Category = "Debug")
		TArray<UCOVStateEffect*> GetAllEffectOnPawn();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug")
		TArray<class UParticleSystemComponent*> ParticalEffects;
	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();
	UFUNCTION()
		void SwitchEffect(UCOVStateEffect* Effect, bool bAddOrRemove);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effect")
	FVector EffectOffset = FVector(0);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Effect")
	float TimeDeathAnim = 1.f;
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	// ������

	UFUNCTION(BlueprintCallable)
		virtual void InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index);
	UFUNCTION(BlueprintCallable)
		virtual void CharacterAttackEvent(bool bIsAttacking);

	// ������
	
	// Change State

	UFUNCTION(BlueprintCallable)
		virtual void CharackterUpdate();
	UFUNCTION(BlueprintCallable)
		virtual void ChangeMovementState();



	// Change State

	// GetSome...()

	UFUNCTION(BlueprintCallable)
		AWeaponBase* GetCurrentWeapon();
	
	// GetSome...()

	//������� ��� ��������

	UFUNCTION(NetMulticast, Reliable)
	void WeaponReloadStart_Multicast(UAnimMontage* AnimReload, float ReUseTime);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP();
	UFUNCTION(NetMulticast, Reliable)
	virtual void WeaponReloadEnd_Multicast(bool IsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool Success);
	UFUNCTION(NetMulticast, Reliable)
	virtual void WeaponUsedStart_Multicast(UAnimMontage* AnimUsingWeapon, float UsingTime);
	virtual void AdditionalActionForWeaponUse();
	virtual void AdditionalActionForWeaponReloadEnd(bool IsSucces, int32 AmmoSafe);

	UFUNCTION()
	void WeaponUseEnd(bool IsSuccess);
	UFUNCTION()
	virtual void DeathCharacter();
	UFUNCTION(BlueprintNativeEvent)
	void DeathCharacter_BP();
	//������� ��� ��������

	//����������
	EPhysicalSurface GetSurfaceType() override;
	TArray<UCOVStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UCOVStateEffect* RemoveEffect) override;
	void AddEffect(UCOVStateEffect* newEffect) override;
	void GetInfoToAttachEmiterEffect(USceneComponent*& Component, FVector(&Offset), FName& AttachedBone) override;
	//����������

	// NET
	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* MontageToPlay);

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);


};
