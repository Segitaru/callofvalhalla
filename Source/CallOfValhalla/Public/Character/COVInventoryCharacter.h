// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "../FuncLibrary/Types.h"
#include "COVInventoryCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAddicionalWeaponInfo, AddicionalWeaponInfo,int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, WeaponType, int32, cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAddicionalWeaponInfo, AddicionalWeaponInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoWeaponEmpty, EWeaponType, EmptyWeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoWeaponAvaible, EWeaponType, EmptyWeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlot, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CALLOFVALHALLA_API UCOVInventoryCharacter : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UCOVInventoryCharacter();

		FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoWeaponEmpty OnAmmoWeaponEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoWeaponAvaible OnAmmoWeaponAvaible;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnUpdateWeaponSlot OnUpdateWeaponSlot;
	//Event current weapon not have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponNotHaveRound OnWeaponNotHaveRound;
	//Event current weapon have additional_Rounds 
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnWeaponHaveRound OnWeaponHaveRound;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponsSlot;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;
	
		int32 MaxSlotWeapon = 5;

	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo, bool bIsForward);
	FAddicionalWeaponInfo GetFAddicionalWeaponInfo(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	void SetAddicionalWeaponInfo(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo);
	UFUNCTION(BlueprintCallable)
	void WeaponChangeAmmo(EWeaponType TypeWeapon, int32 AmmoChange);

	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AviableAmmoToReload);

	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAddicionalWeaponInfo PreviosWeaponInfo);

	//Interface PickUp

	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeapon(int32& FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckWeaponNameInSlot(FWeaponSlot NewWeapon);
	
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropItemFromInventory(int32 IndexSlot, FDropItem& DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void DropWeaponByIndex_OnServer(int32 ByIndex);

	UFUNCTION(BlueprintCallable, Category = "Interface")
		FName GetWeaponNameBySlotIndex(int32 IndexSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetWeaponTypeBySlotIndex(FName IdWeaponName, EWeaponType &WeaponType);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Init")
		void InitInventory_OnServer(const TArray<FWeaponSlot>& InitWeaponsSlot,const TArray<FAmmoSlot>& InitAmmoSlots);

	//NET
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION(NetMulticast, Reliable)
		void OnAmmoChange_Multicast(EWeaponType TypeWeapon, int32 Cout);
	UFUNCTION(NetMulticast, Reliable)
		void OnWeaponAdditionalInfoChange_Multicast(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo);
	UFUNCTION(NetMulticast, Reliable)
		void OnAmmoWeaponEmpty_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable)
		void OnAmmoWeaponAvaible_Multicast(EWeaponType TypeWeapon);
	UFUNCTION(NetMulticast, Reliable)
		void OnUpdateWeaponSlot_Multicast(int32 IndexSlot, FWeaponSlot NewWeapon);
	UFUNCTION(NetMulticast, Reliable)
		void OnWeaponNotHaveRound_Multicast();
	UFUNCTION(NetMulticast, Reliable)
		void OnWeaponHaveRound_Multicast();
	UFUNCTION(Server, Reliable)
		void OnSwitchWeapon_OnServer(FName WeaponIdName, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 NewCurrentIndexWeapon);
	
};