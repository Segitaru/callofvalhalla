// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"

#include "Character/BaseCharacter.h"
#include "BasePlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class CALLOFVALHALLA_API ABasePlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()

protected:

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}

	void PressedWalkCharacter();
	void PressedSprintCharacter();
	void PressedAimCharacter();
	void PressedAttackCharacter();
	void PressedTryCooldownWeapon();
	void PressedNextWeapon();
	void PressedPreviousWeapon();
	void PressedDropWeapon();

	void ReleasedWalkCharacter();
	void ReleasedSprintCharacter();
	void ReleasedAimCharacter();
	void ReleasedAttackCharacter();

public: 
	ABasePlayerCharacter();

	// ���������� ������ ������, ��� ������� 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
		float CameraHeight;
	
	//test
	FTimerHandle MemberTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor)
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor)
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

private: 
	// ���������� � ����������: ������ 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* InteractableSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class UCOVCameraShake> CameraShake;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCOVInventoryCharacter* InventoryCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float CameraAngle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		bool Smooth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float SmoothFactor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float MaxDistanceCamera =1500.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		float MinDistanceCamera = 450.f;
	
	


public:
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent);

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	virtual void InitWeapon(FName IdWeapon, FAddicionalWeaponInfo AddicionalWeaponInfo, int32 Index) override;
	//virtual void WeaponReloadEnd(bool IsSuccess, int32 AmmoSafe) override;
	
	virtual void AdditionalActionForWeaponUse() override;
	virtual void AdditionalActionForWeaponReloadEnd(bool IsSuccess, int32 AmmoSafe) override;

	void DeathCharacter() override;
	/** �������� ������ ������ **/
	UFUNCTION(BlueprintCallable)
	void MakeCameraShake();

	FTimerHandle TimerCameraSmooth;
	void SetTimerSmoothCamera();
	void SmoothingCameraBoom();

	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);


	UDecalComponent* CurrentCursor;
	UFUNCTION(BlueprintCallable)
		class UDecalComponent* GetCursorToWorld();

	UFUNCTION(Server, Reliable)
		void TryReloadWeaponFromPlayer_OnServer();

};