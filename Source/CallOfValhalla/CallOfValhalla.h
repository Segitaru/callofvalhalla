// Copyright ICFHAG Studio. All rights reserved. 2021

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogCOV, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogCOV_Net, Log, All);