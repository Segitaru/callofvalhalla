// Copyright ICFHAG Studio. All rights reserved. 2021

using UnrealBuildTool;
using System.Collections.Generic;

public class CallOfValhallaTarget : TargetRules
{
	public CallOfValhallaTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "CallOfValhalla" } );
	}
}
